# iLab - Ifood
<br/>

O programa iLab tem como objetivo construir o futuro no curto prazo da área de Tecnologia e Data do iFood acelerando pessoas com perfil Junior para atuarem plenamente como um Foodlover.
Os Foodlovers do programa iLab foram organizados em grupos para realizar um desafio que consiste na criação de uma aplicação com microsserviços.
<br />

## 💻 Desenvolvedores do Grupo Acarajava
<br />

<img src="/membros.png"/>

<br />


- [Claudia dos Anjos](https://www.linkedin.com/in/claudia-anjos/)
- [Geraldo Junior](https://www.linkedin.com/in/geraldo-s-s-junior/)
- [Gustavo Nunes](https://www.linkedin.com/in/gustavopnunes/)
- [Lucas Rocha](https://www.linkedin.com/in/lucas-rocha-530618149/)
- [Talita Carvalho](https://www.linkedin.com/in/talita-almeida-carvalho-23735314a/)

## 🚀 Sobre o Desafio
<br />

Desenvolver uma aplicação em Java com três APIs:

- Administrador: para fazer autenticação de administradores;
- Usuários: para cadastrar e listar usuários. Para utilizar esta API é necessário estar autenticado pela aplicação de administradores, passando o token JWT;
- Pedidos: para fazer pedidos de usuários. Para utilizar esta API é necessário estar autenticado pela aplicação de administradores, passando o token JWT;

Incluir mensageira: fazer com que a conclusão do pedido seja feita em background utilizando o sistema de mensageria, Kafka ou SQS e SES enviando um email de pedido realizado com sucesso.

Criar uma pipeline, deployando dados em um kubernetes para cada projeto.

Criar uma aplicação front-end para utilização da API.

Utilizar o New Relic para monitoramento e observability, dos servidores e aplicações.
<br />



### ✒️ Metodologia

Para priorizar a produtividade e a organização das entregas utilizamos o Jira.

[Acesso ao Jira do grupo Acarajava](https://testejirageraldo.atlassian.net/jira/software/projects/DIG/boards/2/roadmap?shared=&atlOrigin=eyJpIjoiYTg0NmZiNjdjMmU5NDU3ZGI2N2EyMmMxM2YyNGFlNzAiLCJwIjoiaiJ9)
<br />

## 📋 Projeto
<br />

Arquitetura Back End do projeto:

<img src="/arquitetura.png"/>

Repositórios do projeto:
<br />

##### 🛠 [API Administrador](https://gitlab.com/ilab-grupo6/api-admin)
 
##### 🛠 [API Usuários](https://gitlab.com/ilab-grupo6/api-user)

##### 🛠 [API Pedidos](https://gitlab.com/ilab-grupo6/api-order)

##### 🛠 [Front End](https://gitlab.com/ilab-grupo6/front-end)

##### 🛠 [SQS-SES](https://gitlab.com/ilab-grupo6/java-sqs-ses)
<br />

## 📋 Agradecimentos
<br />
Agradecemos a Ifood, Foodlover Emmanuel Neri, professor Danilo da Gama Academy e todos que também contribuíram para o desenvolvimento do projeto. 
